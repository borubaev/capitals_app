import 'package:capitals/constants/appColors.dart';
import 'package:capitals/pages/HomePage.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          scaffoldBackgroundColor: AppColors.scafColor,
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurpleAccent),
          useMaterial3: true,
          sliderTheme:
              SliderThemeData(thumbShape: SliderComponentShape.noOverlay)),
      home: const HomePage(),
    );
  }
}
