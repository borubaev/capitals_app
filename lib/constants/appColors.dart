import 'package:flutter/material.dart';

class AppColors {
  static const blue = Colors.blue;
  static const appBarColor = Color(0xfff0f0f0);
  static const scafColor = Color(0xfffafafa);
  static const black = Colors.black;
  static const dividColor = Color.fromARGB(255, 43, 41, 41);
  static const white = Colors.white;
  static const red = Colors.red;
  static const green = Colors.green;
}
