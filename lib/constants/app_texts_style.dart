import 'package:capitals/constants/appColors.dart';
import 'package:flutter/material.dart';

class AppText {
  static const TextStyle num1 = TextStyle(
      color: AppColors.red, fontSize: 18, fontWeight: FontWeight.w700);
  static const TextStyle num2 = TextStyle(
      color: AppColors.black, fontSize: 18, fontWeight: FontWeight.w500);
  static const TextStyle num3 = TextStyle(
      color: AppColors.green, fontSize: 18, fontWeight: FontWeight.w700);
  static const TextStyle city = TextStyle(
      color: AppColors.black, fontSize: 26, fontWeight: FontWeight.w400);
}
