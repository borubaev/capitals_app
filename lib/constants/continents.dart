import 'package:flutter/material.dart';

class Continents {
  static const List<String> continents = [
    'Africa',
    'Asia',
    'Australia',
    'Europe',
    'North_America',
    'South_America',
  ];
  static List<MaterialColor> contColors = [
    Colors.orange,
    Colors.green,
    Colors.red,
    Colors.yellow,
    Colors.blue,
    Colors.brown,
  ];
}
