class Suroo {
  const Suroo({required this.text, required this.image, required this.jooptor});
  final String text;
  final String image;
  final List jooptor;
}

class joop {
  const joop({required this.text, required this.tuuraJoop});
  final String text;
  final bool tuuraJoop;
}

const Suroo s1 = Suroo(
  text: 'Paris',
  image: 'Paris',
  jooptor: [
    joop(text: 'Germany', tuuraJoop: false),
    joop(text: 'Poland', tuuraJoop: false),
    joop(text: 'France', tuuraJoop: true),
    joop(text: 'Russia', tuuraJoop: false),
  ],
);
const Suroo s2 = Suroo(
  text: 'Muenchen',
  image: 'Muenchen',
  jooptor: [
    joop(text: 'Canada', tuuraJoop: false),
    joop(text: 'Italy', tuuraJoop: false),
    joop(text: 'France', tuuraJoop: false),
    joop(text: 'Bavaria, Gremany', tuuraJoop: true),
  ],
);
const Suroo s3 = Suroo(
  text: 'Rome',
  image: 'Rome',
  jooptor: [
    joop(text: 'Italy', tuuraJoop: true),
    joop(text: 'Belarusia', tuuraJoop: false),
    joop(text: 'Litva', tuuraJoop: false),
    joop(text: 'Ukraine', tuuraJoop: false),
  ],
);
const Suroo s4 = Suroo(
  text: 'Tokyo',
  image: 'Tokyo',
  jooptor: [
    joop(text: 'China', tuuraJoop: false),
    joop(text: 'Korea', tuuraJoop: false),
    joop(text: 'Japan', tuuraJoop: true),
    joop(text: 'Indonesia', tuuraJoop: false),
  ],
);
const Suroo s5 = Suroo(
  text: 'Pekin',
  image: 'Pekin',
  jooptor: [
    joop(text: 'Canada', tuuraJoop: false),
    joop(text: 'China', tuuraJoop: true),
    joop(text: 'America', tuuraJoop: false),
    joop(text: 'Malaysia', tuuraJoop: false),
  ],
);

List<Suroo> victorina = [
  s1,
  s2,
  s3,
  s4,
  s5,
];
