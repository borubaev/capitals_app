import 'dart:math';

import 'package:capitals/constants/appColors.dart';
import 'package:capitals/constants/continents.dart';
import 'package:capitals/pages/testPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.appBarColor,
        title: const Text(
          'Мамлекеттер борбору',
          style: TextStyle(color: AppColors.black),
        ),
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.settings),
            color: AppColors.blue,
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_vert),
            color: AppColors.black,
          )
        ],
      ),
      body: Column(
        children: [
          const Divider(
            indent: 15,
            endIndent: 15,
            color: AppColors.dividColor,
          ),
          Expanded(
            child: GridView.builder(
                padding: EdgeInsets.only(left: 10, top: 15, right: 10),
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: 6,
                itemBuilder: (BuildContext ctx, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TestView(),
                        ),
                      );
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Column(
                        children: [
                          Text(Continents.continents[index]),
                          const SizedBox(
                            height: 17,
                          ),
                          SvgPicture.asset(
                            "assets/continents/${Continents.continents[index]}.svg",
                            width: 100,
                            height: 100,
                            color: Continents.contColors[index],
                          )
                          // Image.asset('assets/continents/africa.svg')
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
