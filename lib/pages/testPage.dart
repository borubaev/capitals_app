import 'package:capitals/constants/appColors.dart';
import 'package:capitals/constants/app_texts_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../model/victorina.dart';

class TestView extends StatefulWidget {
  const TestView({super.key});

  @override
  State<TestView> createState() => _TestViewState();
}

class _TestViewState extends State<TestView> {
  List<Suroo> test = victorina;
  int indexTest = 0;
  int tuura = 0;
  int kata = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: AppColors.appBarColor,
        actions: [
          Container(
            width: 80,
            height: 30,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: AppColors.scafColor,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.7),
                      blurRadius: 7,
                      offset: const Offset(0, 3)),
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  '$kata',
                  style: AppText.num1,
                ),
                Text(
                  '32',
                  style: AppText.num2,
                ),
                Text(
                  '$tuura',
                  style: AppText.num3,
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 60,
          ),
          const Text(
            '3',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            width: 90,
          ),
          const Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          const Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          const Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          const Icon(
            Icons.more_vert,
          ),
        ],
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Slider(
            value: indexTest.toDouble(),
            onChanged: (value) {},
            max: 26,
            min: 0,
          ),
          const SizedBox(
            height: 30,
          ),
          Text(
            '${test[indexTest].text}',
            style: AppText.city,
          ),
          Container(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child:
                  Image.asset('assets/capitals/${test[indexTest].text}.jpeg'),
            ),
          ),
          Expanded(
            child: GridView.builder(
              padding: const EdgeInsets.only(left: 5, right: 5),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 1.6),
              itemCount: 4,
              itemBuilder: (BuildContext ctx, index) {
                return Card(
                  color: Colors.grey,
                  child: InkWell(
                    onTap: () {
                      print(indexTest);
                      if (indexTest >= test.length - 1) {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: Text('Sizdin test jyiyntyktaldy!'),
                            content: Text(
                              'Tuura jooptor $tuura \ Kata jooptor $kata',
                            ),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  kata = 0;
                                  tuura = 0;
                                  indexTest = 0;
                                  setState(() {});
                                  Navigator.pop(context);
                                },
                                child: Text('Cancel'),
                              ),
                            ],
                          ),
                        );
                      } else {
                        if (test[indexTest].jooptor[index].tuuraJoop) {
                          tuura++;
                        } else {
                          kata++;
                        }
                        setState(() {
                          indexTest++;
                        });
                      }
                    },
                    child: Center(
                      child: Text('${test[indexTest].jooptor[index].text}'),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
